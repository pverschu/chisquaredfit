from ChiSquaredFit import *

SM_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_SM_pttt.root");
cqq1_2_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_253_pttt.root");
cqq1_5_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_501_pttt.root");
cqq1_m2_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_m253_pttt.root");
cqq1_m5_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/CombineHists/SMEFT_cQQ1_m501_pttt.root");

var_name = "truth"

hist_SM = SM_file.Get(var_name)
hist_2 = cqq1_2_file.Get(var_name)
hist_5 = cqq1_5_file.Get(var_name)
hist_m2 = cqq1_m2_file.Get(var_name)
hist_m5 = cqq1_m5_file.Get(var_name)


parameter_list = ['cqq1']
func_form = 'pol'

temp = TemplateFitter(parameter_list, func_form)

temp.add_template([0.0], hist_SM)
temp.add_template([25.3], hist_2)
temp.add_template([50.1], hist_5)
temp.add_template([-25.3], hist_m2)
temp.add_template([-50.1], hist_m5)

temp.fit()


unfolded_file = ROOT.TFile("/afs/cern.ch/work/p/pverschu/private/TopXSec/Unfolding/1DMC/unfolded_hists/pttt_svd_10_SMresp_SMunfold.root")

unfolded = unfolded_file.Get("unfolded")
unfolded_cov = unfolded_file.Get("cov")              

for i in range(unfolded.GetNbinsX()):
    deltax = unfolded.GetBinLowEdge(i+2) - unfolded.GetBinLowEdge(i+1)
    unfolded.SetBinContent(i+1, unfolded.GetBinContent(i+1)/deltax)
    
    for j in range(unfolded.GetNbinsX()):
        unfolded_cov.SetBinContent(i+1, j+1, unfolded_cov.GetBinContent(i+1, j+1)/(deltax**2) )

chi = ChiSquaredFit(temp,unfolded,unfolded_cov)

paramMin, chiSqMinimum = chi.minimize()

print(paramMin)
print(chiSqMinimum)

# chi.printDeltaChiSqScan([4,4.6], chiSqMinimum)

# chi.printAsimovDeltaChiSqScan([-0.5,0.5],[0.])

chi.plotDeltaChiSqScan([4,4.6], chiSqMinimum)

chi.plotAsimovDeltaChiSqScan([-0.5,0.5],[0.])
