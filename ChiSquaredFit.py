import ROOT

import sys
sys.path.append('/afs/cern.ch/work/p/pverschu/private/TopXSec/XSec_Pol_Fit/TemplateFitter/')

import matplotlib.pyplot as plt
from scipy.optimize import minimize
from TemplateFitter import *
import numpy as np
from numpy.linalg import inv

class ChiSquaredFit:

    def __init__(self, templatefitter, obs, cov):

        # Define the dimension
        self._obs_dim = 0

        # Check if the passed object is TemplateFitter class.
        if not (str(templatefitter)[1:15] == "TemplateFitter"):
            print("Please pass a TemplateFitter object as argument.")
            exit(0)
        else:
            self._obs_dim = templatefitter._template_dim

        # Save the TemplateFitter
        self._TemplateFitter = templatefitter

        # Check if the passed histograms are TH1 classes.
        if not (obs.InheritsFrom(ROOT.TH1.Class())):
            print("Please pass a TH1 object as observed data.")
            exit(0)            

        if not (cov.InheritsFrom(ROOT.TH2.Class())):
            print("Please pass a TH2 object as covariance matrix.")
            exit(0)            

            
        # Check if the passed objects have the right dimensions.
        if not (self._obs_dim == obs.GetNbinsX()):
            print("The passed observed data histogram has "+str(obs.GetNbinsX())+" bins. The TemplateFitter has "+str(self._obs_dim)+" bins. Please pass objects with the same dimensions.")
            exit(0)

        if not (self._obs_dim == cov.GetNbinsX() and self._obs_dim == cov.GetNbinsY()):
            print("The passed covariance matrix has "+str(cov.GetNbinsX())+"x"+str(cov.GetNbinsY())+" bins. The TemplateFitter and observed data histogram have "+str(self._obs_dim)+" bins. Please pass objects with the same dimensions.")
            exit(0)

        self._cov = np.zeros((self._obs_dim,self._obs_dim))
        self._obs_vec = np.zeros(self._obs_dim)

        for i in range(self._obs_dim):
            
            self._obs_vec[i] = obs.GetBinContent(i+1)

            for j in range(self._obs_dim):
                
                self._cov[i,j] = cov.GetBinContent(i+1, j+1)

        self._invcov = inv(self._cov)
        

    def ChiSquared(self,params):
        
        pred_vec = self._TemplateFitter.predict([params[0]])
        
        pred_obs_vec = np.array(pred_vec) - self._obs_vec

        invcov_pred = self._invcov.dot(pred_obs_vec)

        chisq = pred_obs_vec.dot(invcov_pred)

        return chisq

    def AsimovChiSquared(self,params,param_point=[0.]):
        
        pred_vec = self._TemplateFitter.predict([params[0]])
        asimov_vec = self._TemplateFitter.predict(param_point)

        pred_obs_vec = np.array(pred_vec) - np.array(asimov_vec)

        invcov_pred = self._invcov.dot(pred_obs_vec)

        chisq = pred_obs_vec.dot(invcov_pred)

        return chisq


    def minimize(self):
        init_param = [0.]
        
        res = minimize(self.ChiSquared,init_param, method='Nelder-Mead')
        
        print("Minimum for "+str(self._TemplateFitter._parameters[0])+" found at "+str(res.x[0]))
        
        self._minimum = res.x[0]
        self._chiSqMinimum = res.fun

        return self._minimum, self._chiSqMinimum

    def printDeltaChiSqScan(self, param_range, chiSqNull = 0):
        
        n_points = 100

        param_scan_range = np.linspace(param_range[0], param_range[1], n_points)

        deltaChiSq = []

        for i in range(n_points):
            print("Delta chi2 for "+str(self._TemplateFitter._parameters[0])+"="+str(param_scan_range[i])+" "+str(self.ChiSquared([param_scan_range[i]]) - chiSqNull))
            deltaChiSq.append(self.ChiSquared([param_scan_range[i]]) - chiSqNull)


        return deltaChiSq

    def printAsimovDeltaChiSqScan(self, param_range, param_point = [0.]):
        
        n_points = 100

        param_scan_range = np.linspace(param_range[0], param_range[1], n_points)
        
        deltaChiSq = []

        for i in range(n_points):
            print("Delta chi2 for "+str(self._TemplateFitter._parameters[0])+"="+str(param_scan_range[i])+" "+str(self.AsimovChiSquared([param_scan_range[i]],param_point) - self.AsimovChiSquared(param_point, param_point)))
            deltaChiSq.append(self.AsimovChiSquared([param_scan_range[i]],param_point) - self.AsimovChiSquared(param_point, param_point))

        return deltaChiSq

    def plotDeltaChiSqScan(self, param_range, chiSqNull = 0):
        
        n_points = 100

        param_scan_range = np.linspace(param_range[0], param_range[1], n_points)

        deltaChiSq = []
        

        for i in range(n_points):
            deltaChiSq.append(self.ChiSquared([param_scan_range[i]]) - chiSqNull)

        plt.clf()

        plt.plot(param_scan_range, deltaChiSq, 'r-')
        plt.xlabel(str(self._TemplateFitter._parameters[0]))
        plt.ylabel("delta chi")
        plt.savefig("deltachi.png")

    def plotAsimovDeltaChiSqScan(self, param_range, param_point = [0.]):
        
        n_points = 100

        param_scan_range = np.linspace(param_range[0], param_range[1], n_points)
        
        deltaChiSq = []

        for i in range(n_points):
            deltaChiSq.append(self.AsimovChiSquared([param_scan_range[i]],param_point) - self.AsimovChiSquared(param_point, param_point))

        plt.clf()

        plt.plot(param_scan_range, deltaChiSq, 'r-')
        plt.xlabel(str(self._TemplateFitter._parameters[0]))
        plt.ylabel("delta chi")
        plt.savefig("asimovdeltachi.png")
        
